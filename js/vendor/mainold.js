'use strict';
(function($) {
    var $mobileMenu = $(".hamburger"),
        $closeBtn = $(".closebtn"),
        $navigation = $('.navigation'),
        $inSiteNavigation = $(".navigation li a"),
        $container = $('#container'),
        $footer = $('footer'),
        $body = $('.closebtn'),
        $grids = $('.front-back'),
        $body = $('body'),
        userHasScrolled = false,
        $headerContent = $('.headerContent'),
        windowWidth = $(window).width(),
        $navtoScreenRatio = 0.4;


    // getRandomUnique();
    doFlipping(getRandomUnique());
    // $grids.mouseout(function(e) {
    //     var indexed = 2;
    //     console.log($(this).parent())
    //     var thisTargetIndex = $(this).parent().index();
    //     rotateGrids(thisTargetIndex);
    // });
    // $grids.mouseover(function(e) {
    //     var thisTargetIndex = $(this).parent().index();;

    //     var thisTarget = $(this);
    //     rotateGrids(thisTargetIndex);
    // });
    // $('.front-back')
    function doFlipping(flippArr) {
        console.log(getRandomUnique());
        for (var i = 0; i < flippArr.length; i++) {
            console.log(flippArr[i]);
            flipBack(i, flippArr)

            // setTimeout(function() {
            //     rotateGrids(flippArr[i]);
            // }, i * 2000);
        }
    }

    function flipBack(num, arr) {
        console.log("num is" + num);
        setTimeout(function() {
            rotateGrids(arr[num]);
        }, num * 1000);
        // flipFront
        setTimeout(function() {
            rotateGrids(arr[num]);
        }, num * 1500);
        if (num === arr.length) {
            setTimeout(function() {
                doFlipping(arr);
            }, num * 2000);

        }
    }

    function getRandomUnique() {
        var limit = 6,
            amount = 3,
            lower_bound = 0,
            upper_bound = 5,
            unique_random_numbers = [];

        if (amount > limit) limit = amount; //Infinite loop if you want more unique
        //Natural numbers than existemt in a
        // given range
        while (unique_random_numbers.length < limit) {
            var random_number = Math.round(Math.random() * (upper_bound - lower_bound) + lower_bound);
            if (unique_random_numbers.indexOf(random_number) == -1) {
                // Yay! new random number
                unique_random_numbers.push(random_number);
            }
        }
        return unique_random_numbers;
    }

    function rotateGrids(target) {
        var checkForFlip = $grids.eq(target).hasClass("flipped");

        if (!checkForFlip) {
            $grids.eq(target).removeClass("unFlip");
            $grids.eq(target).addClass("flipped ");
        } else {
            $grids.eq(target).removeClass("flipped");
            $grids.eq(target).addClass("unFlip");
        }
    }
    // mobile menu on -off
    $mobileMenu.click(function() {

        openNav()
        return false;
    });
    $closeBtn.click(function() {
        closeNav()
        return false;
    });

    function openNav() {
        $navigation.width($(window).width() * $navtoScreenRatio);
        $mobileMenu.hide();


        setTimeout(function() {
            var mrg = $navigation.width(),
                containerPercentage = (100 * (1 - $navtoScreenRatio)) + "%";
            $container.width($(window).width() - $navigation.width());

            $container.css('margin-left', mrg + 'px');
            // console.log(containerPercentage);
            $container.css('width', containerPercentage);
            // $container.css('margin-left', mrg + 'px')


        }, 100)
    }

    function closeNav() {


        setTimeout(function() {
            var mrg = $navigation.width();
            $container.removeAttr("style");
            $mobileMenu.removeAttr("style");
            $navigation.removeAttr("style");
        }, 100)
    }
    // mobile menu on -off end

    // check for scroll
    window.onscroll = function(e) {
        // called when the window is scrolled.  

        var scroll = $(window).scrollTop();

        if (scroll > 150) {
            userHasScrolled = true;
            $headerContent.css("background-color", "#404040");
            $headerContent.css("padding-top", "0");
            $headerContent.css("transition", "all 0.5s");
        } else {
            userHasScrolled = false;
            $headerContent.css("background-color", "transparent");
            $headerContent.css("padding-top", "1.5em")

            $headerContent.css("transition", "all 0.5s");
        }
    }


    $inSiteNavigation.click(function() {



        if (windowWidth <= 768) {
            closeNav();
            // move all body
            $('html, body').animate({
                scrollTop: $($(this).attr('href')).offset().top - 500
            }, 500);
            return false;
        } else {
            // move all navigations
            $('html, body').animate({
                scrollTop: $($(this).attr('href')).offset().top - 100
            }, 500);
            return false;
        }


    });

})(jQuery);